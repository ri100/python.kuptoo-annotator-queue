#!/usr/bin/env bash

sudo cp annotatord.service /etc/systemd/system/annotatord.service
sudo chown root:root /etc/systemd/system/annotatord.service
sudo chmod 664 /etc/systemd/system/annotatord.service

sudo cp persisterd.service /etc/systemd/system/persisterd.service
sudo chown root:root /etc/systemd/system/persisterd.service
sudo chmod 664 /etc/systemd/system/persisterd.service

sudo systemctl daemon-reload

