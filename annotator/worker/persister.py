import logging, pytz, config
from kombu import Exchange, Queue
from kombu.mixins import ConsumerMixin
from elastic.storage import Elastic
from datetime import datetime, timezone

logger = logging.getLogger('persister-worker')


class PersisterWorker(ConsumerMixin):

    def __init__(self, connection, index, skip_existing=False):
        self.skip_existing = skip_existing
        exchange = Exchange(config.rabbit['persist.queue'], 'direct', durable=True)
        self.queue = Queue(config.rabbit['persist.queue'], exchange=exchange, routing_key='offer.TAG')
        self.storage = Elastic(config.elastic, index)
        self.connection = connection
        self.batch_size = 1000
        self.all_records = 0
        self._reset()
        self.skipped = 0
        logger.debug("Batch size set to {}".format(self.batch_size))

    def _reset(self):
        self.batch_counter = self.batch_size
        self.batch = []

    def get_consumers(self, Consumer, channel):
        return [
            # There may be several consumers
            Consumer(queues=[self.queue], callbacks=[self.handle_offer],
                     auto_declare=True, prefetch_count=20,
                     tag_prefix='persister-worker-'),
        ]

    def _exists(self, id):
        return id in self.storage

    def handle_offer(self, payload_in, message):
        for row in payload_in:
            self.all_records += 1

            raw_hash = row['offer']['hash']['raw']

            self.batch_counter -= 1

            if self.skip_existing and self._exists(raw_hash):
                self.skipped += 1
                continue

            today = datetime.now(timezone.utc)
            cet = pytz.timezone('Europe/Warsaw')

            row['meta'] = {
                'active': False,
                'inserted': today.astimezone(cet).strftime("%Y-%m-%d %H:%I:%S %Z")
            }

            self.batch.append(row)

            if self.batch_counter <= 0:
                r = self.storage.insert(self.batch)
                logger.debug("Index result {}".format(r))
                self._reset()
                self.skipped = 0

        message.ack()

    def close(self):
        logging.info('Saving remaining: {} ...'.format(len(self.batch)))
        self.storage.insert(self.batch)
        self._reset()
