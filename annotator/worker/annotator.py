import hashlib, logging, time, config
from annotator_nlp.preprocessing import standardize, tokenize_sentence
from kombu import Exchange, Queue, Connection
from kombu.mixins import ConsumerMixin
from kuptoo_annotator.annotator import Annotator
from kuptoo_annotator.classifier import Classifier, CategoryPath
from kuptoo_annotator.corrector import Corrector
from kuptoo_annotator.labeler import Labeler
from kuptoo_annotator.model.context_model import ContextModel
from kuptoo_annotator.model.pos_model import PosModel
from kuptoo_annotator.utils.stemmer import stem
from kuptoo_annotator.utils.tags import basic_tag_keys

logger = logging.getLogger('annotator-worker')


class AnnotationHandler:

    def __init__(self, remapping: dict = None):
        self.vectorizer = ContextModel().get_latent_encoder()
        self.classifier = Classifier(ContextModel())
        self.labeler = Labeler(min_probability=.4)
        self.annotator = Annotator(PosModel(), remapping)

    @staticmethod
    def _get_hash(string):
        h = hashlib.md5()
        h.update(string.encode('utf-8'))
        return h.hexdigest()

    @staticmethod
    def _standardize(text):
        text = standardize(text.lower())
        return " ".join(tokenize_sentence(text, True))

    @staticmethod
    def _transform_label(key, labels):
        return {
            'id': [label['id'] for label in labels[key]],
            'name': [label['name'][0] for label in labels[key]],
            'probability': [label['probability'] for label in labels[key]],
        }

    def handle(self, payload_in):
        standardized_texts = [self._standardize(row['offer']['title']['raw']) for row in payload_in]

        labels_vectors = list(self.labeler.label(standardized_texts))
        category_paths = list(self.classifier.categorize(standardized_texts))  # type: list[CategoryPath]
        tags = self.annotator.tag(standardized_texts, allowed_tags=basic_tag_keys)
        tags = list(Corrector().correct(tags))
        stems = [stem(t) for t in tags]

        payload_out = []
        for position, row in enumerate(payload_in):
            title = row['offer']['title']['raw']
            std = standardized_texts[position]

            hash = {
                'hash': {
                    'raw': self._get_hash(title),
                    'std': self._get_hash(std)
                }
            }

            title = {
                'title': {
                    'raw': title,
                    'std': std
                }
            }

            labels = labels_vectors[position]
            # labels = [[label['id'], label['name'], float("{0:.1f}".format(label['probability'] * 100))] for
            #           label in labels]

            _labels = {'labels': {}}
            for key in ['context', 'products', 'action', 'rel']:
                _labels['labels'][key] = self._transform_label(key, labels)

            # _labels = {
            #     'label': {
            #         'id': [label['id'] for label in labels],
            #         'name': [label['name'] for label in labels],
            #         'probability': [float("{0:.1f}".format(label['probability'] * 100)) for label in labels],
            #     }
            # }

            category_path, vector = category_paths[position]  # type: CategoryPath, ndarray
            _category = {
                'category': {
                    'id': [int(cat.cidx) for cat in category_path.path[1:]],
                    'name': [cat.name for cat in category_path.path[1:]],
                    'probability': [float(cat.probability) for cat in category_path.path[1:]],
                    'valid': [bool(category_path.verify(1)), bool(category_path.verify(2))],
                    'vector': vector.tolist()
                }
            }
            context = category_path.path[0]
            _context = {
                'context': {
                    'id': int(context.cidx),
                    'name': context.name,
                    'probability': float(context.probability),
                }
            }

            if tags[position]:
                auto_tags = {
                    'tags': {
                        'auto': tags[position],
                        'stem': stems[position]
                    }
                }
                row['offer'].update(auto_tags)

            row['offer'].update(title)
            row['offer'].update(_labels)
            row['offer'].update(_category)
            row['offer'].update(_context)
            row['offer'].update(hash)

            payload_out.append(row)

        return payload_out


class AnnotatorWorker(ConsumerMixin):

    def __init__(self, connection, progress=0, remapping: dict = None):
        self._handler = AnnotationHandler(remapping)
        self.progress = progress
        self.i = 0
        self.time = time.time()
        self.exchange_insert = Exchange(config.rabbit['insert.queue'], 'direct', durable=True)
        self.queue_insert = Queue(config.rabbit['insert.queue'], exchange=self.exchange_insert, routing_key='offer.NEW')
        self.connection = connection
        self.exchange_tags = Exchange(config.rabbit['persist.queue'], 'direct', durable=True)
        self.queue_tags = Queue(config.rabbit['persist.queue'], exchange=self.exchange_tags, routing_key='offer.TAG')
        logger.debug('Constructor ends...')

    def get_consumers(self, consumer, channel):
        logger.debug('Consumers loaded...')
        return [
            consumer(queues=[self.queue_insert], callbacks=[self.handle_offer],
                     auto_declare=True, prefetch_count=20,
                     tag_prefix='annotator-worker-'),
        ]

    def handle_offer(self, payload_in, message):
        with Connection(config.rabbit['host'], connect_timeout=120) as local_connection:
            payload_out = self._handler.handle(payload_in)

            self.i += 1

            # Queue
            producer = local_connection.Producer(serializer='json')
            producer.publish(payload_out,
                             exchange=self.exchange_tags, routing_key='offer.TAG',
                             declare=[self.queue_tags])  # , compression='bzip2'
        message.ack()

        if self.progress > 0 and self.i % self.progress == 0:
            elapsed = time.time() - self.time
            logger.info("Average time: {},".format(self.i / elapsed))
