def find_by_id(id):
    return {
        "query": {
            "terms": {
                "_id": id
            }
        }
    }


def match_text(text):
    return {
        "query": {
            "match": {
                "offer.title.std": text
            }
        }
    }


def match_all():
    return {
        "query": {
            "match_all": {}
        }
    }


def match_prod1_tag(text, type):
    return {
        "query": {
            "term": {
                "offer.tags." + type: text
            }
        }
    }


def match1(query, bool_type):
    q = {
        "query": {
            "bool": {
                bool_type: [

                ],
                'must': [

                ]
            }

        }
    }

    words = []
    for rel, data in query.items():
        for value in data:
            words.append(value)
            if rel in ['nieznany']:
                continue
            q['query']['bool'][bool_type].append({
                "term": {
                    "offer.tags." + rel: value
                }
            })

    q['query']['bool']['must'].append({
        "match": {
            "offer.title.std": " ".join(words)
        }
    })

    return q


def match2(query):
    q = {
        "query": {
            "bool": {
                'filter': [],
                'should': []
            }

        }
    }

    words = []
    for rel, data in query.items():
        for value in data:
            words.append(value)
            if rel in ['nieznany']:
                continue
            q['query']['bool']['filter'].append({
                "term": {
                    "offer.tags." + rel: value
                }
            })

    q['query']['bool']['should'].append({
        "match": {
            "offer.title.std": " ".join(words)
        }
    })

    return q
