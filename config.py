import os

rabbit = {
    'host': os.environ[
        'KUPTOO_RABBIT_HOST'] if 'KUPTOO_RABBIT_HOST' in os.environ else 'amqp://guest:guest@127.0.0.1:5672//',
    'insert.queue': os.environ[
        'KUPTOO_RABBIT_INSERT_QUEUE'] if 'KUPTOO_RABBIT_INSERT_QUEUE' in os.environ else 'kuptoo.insert',
    'persist.queue': os.environ[
        'KUPTOO_RABBIT_WRITE_QUEUE'] if 'KUPTOO_RABBIT_WRITE_QUEUE' in os.environ else 'kuptoo.persister'
}

elastic = {
    'host': os.environ['KUPTOO_ELASTIC_HOST'] if 'KUPTOO_ELASTIC_HOST' in os.environ else '127.0.0.1',
    'port': os.environ['KUPTOO_ELASTIC_PORT'] if 'KUPTOO_ELASTIC_PORT' in os.environ else 9200,
    'index': os.environ['KUPTOO_ELASTIC_INDEX'] if 'KUPTOO_ELASTIC_INDEX' in os.environ else 'corpus-allegro-00000001-01'
}
