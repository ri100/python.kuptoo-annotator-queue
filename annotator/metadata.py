from collections import defaultdict
from annotatelib.annotation_service import Annotator


class Title:

    def __init__(self):
        self.context_predictor = Predictor()
        self.annotator = Annotator()

    @staticmethod
    def accuracy_levels(vector, min_level=0.0):
        levels = defaultdict(list)
        for i, prob in enumerate(vector):
            if prob > min_level:
                key = int(prob * 10)
                levels["boost_{}".format(key)].append(i)
        return levels

    def context(self, titles):
        predicted_contexts = self.context_predictor.predict(titles)
        context_levels_list = []
        for position, _ in enumerate(titles):
            context_levels = self.accuracy_levels(predicted_contexts[position], min_level=0.01)
            context_levels_list.append(dict(context_levels))
        return context_levels_list

    def tag(self, titles):
        tags = []
        for title in titles:
            # Already standardized
            product_description = self.annotator.annotate(title, title_standardize=False, noise=True)
            tags.append((product_description, product_description.smart_exclude()))
        return tags


if __name__ == "__main__":
    import pprint
    t = Title()
    titles = ['wiatraki pokojowe elektryczne', 'lampa stołowa ledowa']
    c = t.context(titles)
    ta = t.tag(titles)
    pprint.pprint(c)
    pprint.pprint(ta)
