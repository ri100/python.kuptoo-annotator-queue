import json
from elasticsearch import Elasticsearch
from elastic.utils import ElasticIndex


def load_json(file):
    with open(file, 'r') as f:
        return f.read()


es = Elasticsearch([{'host': '192.168.1.230', 'port': 9200}])
idx = ElasticIndex(es, 'providers')
body = load_json('../indices/providers.json')
body = json.loads(body)
result = idx.create(body['body'], force=True)
print(result)
