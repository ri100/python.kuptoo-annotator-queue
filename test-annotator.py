import logging

from kuptoo_annotator.annotator import Annotator
from kuptoo_annotator.model import NoPosModel
from kuptoo_annotator.utils.stemmer import stem

model = NoPosModel()
annotator = Annotator(model)
model.summary()


logging.basicConfig(level=logging.DEBUG)

try:
    while True:
        title = input('>')
        if title != '':
            tags = annotator.tag([title])
            print(list(tags))
            stems = [stem(tag) for tag in tags]
            print(stems)

except KeyboardInterrupt:
    print('Bye bye')
