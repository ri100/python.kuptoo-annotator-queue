import logging
from kombu import Exchange, Queue, Connection
from tqdm import tqdm
from config import rabbit
from utils.file import JsonFile, count_lines

exchange = Exchange(rabbit['insert.queue'], 'direct', durable=True)
offer_queue = Queue(rabbit['insert.queue'], exchange=exchange, routing_key='offer.NEW')

logging.basicConfig(format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def errback(exc, interval):
    logger.debug('Error: %r', exc, exc_info=1)
    logger.info('Error: {}', str(exc))
    logger.info('Retry in %s seconds.', interval)


try:
    with Connection(rabbit['host'], connect_timeout=120) as conn:

        producer = conn.Producer(serializer='json', auto_declare=True)
        publish = conn.ensure(producer, producer.publish, errback=errback, max_retries=3)

        file = '/mnt/data/corpus_annotated/dataset-2/test.json'

        i, batch = 0, 500
        skip = 0
        payload = []
        for title, images, price in tqdm(JsonFile(file, output=['title', 'images', 'price']),
                                         total=count_lines(file)):

            i += 1

            if i < skip:
                continue

            if 'std' not in title:
                continue

            title = title['std']

            load = {
                "offer": {
                    'title': {
                        'raw': title,
                    },
                }
            }

            if images:
                images = {
                    "photos": {
                        "remote": {
                            "default": 0,
                            "urls": images
                        }
                    }
                }

                load['offer'].update(images)

            if price:
                price = {
                    "price": {
                        "gross": {
                            "amount": price,
                            "currency": "PLN"
                        }
                    }
                }

            payload.append(load)
            if i % batch == 0:
                publish(payload,
                        retry=True,
                        exchange=exchange, routing_key='offer.NEW',
                        declare=[offer_queue])  # , compression='bzip2'

                payload = []

        if payload:
            publish(payload, retry=True, exchange=exchange, routing_key='offer.NEW',
                    declare=[offer_queue])  # , compression='bzip2'
            payload = []

except TimeoutError as e:
    logger.debug(str(e), exc_info=1)
    logger.info(str(e))
except OSError as e:
    logger.debug(str(e), exc_info=1)
    logger.info(str(e))
