import argparse, config, logging
from time import sleep
from elasticsearch.exceptions import ConnectionTimeout
from kombu import Connection
from annotator.worker.persister import PersisterWorker, logger as persister_logger

elastic_worker_logger = logging.getLogger('elastic-worker')

parser = argparse.ArgumentParser(description='Elastic persister worker.')
parser.add_argument('--debug', help="Add debug information", required=False, action='store_true')
args = parser.parse_args()

if args.debug:
    logging.getLogger('elasticsearch').setLevel(logging.INFO)
    persister_logger.setLevel(logging.DEBUG)
    elastic_worker_logger.setLevel(logging.DEBUG)
    elastic_worker_logger.info("Debugging is ON...")
else:
    elastic_worker_logger.setLevel(logging.INFO)


while True:
    try:
        with Connection(config.rabbit['host'], connect_timeout=20) as conn:
            elastic_worker_logger.info('Starting...')
            worker = PersisterWorker(conn, 'kuptoo-allegro-12')
            try:
                worker.run()
            except KeyboardInterrupt:
                worker.close()
                elastic_worker_logger.info('Bye bye')
                exit()
    except ConnectionTimeout as e:
        print(str(e))
        sleep(2)
