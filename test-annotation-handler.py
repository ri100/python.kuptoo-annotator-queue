from pprint import pprint

from annotator.worker.annotator import AnnotationHandler

ah = AnnotationHandler()
rows = ah.handle([
    {
        "offer":
            {
                "title":
                    {
                        "std": "kosiarka elektryczna",
                        "raw": "kosiarka elektryczna"
                    }
            }
    },
    {
        "offer":
            {
                "title":
                    {
                        "std": "ŻWIREK DREWNIANY PODŁOŻE DLA KOTA GRYZONI KRÓLIKA".lower(),
                        "raw": "ŻWIREK DREWNIANY PODŁOŻE DLA KOTA GRYZONI KRÓLIKA".lower()
                    }
            }
    }
])

pprint(rows)
