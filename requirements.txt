elasticsearch
#pymongo
tqdm
kombu==4.2.1
pytz
elasticsearch-dsl>=7.0.0,<8.0.0
#git+https://ri100@bitbucket.org/ri100/python.kuptoo-annotator.git
