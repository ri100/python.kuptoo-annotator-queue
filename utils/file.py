import json


def count_lines(file):
    def blocks(files, size=65536):
        while True:
            b = files.read(size)
            if not b: break
            yield b

    with open(file, "r", encoding="utf-8", errors='ignore') as f:
        return sum(bl.count("\n") for bl in blocks(f))


class JsonFile(object):
    """
    Generator: yields data form json file in form of tuple
    """

    def __init__(self, file_name, output: list, filter=None):
        self.filter = filter
        self.output = output
        self.file_name = file_name

    def __iter__(self):
        with open(self.file_name) as file:
            for line in file:
                try:
                    obj = json.loads(line)

                    if self.filter is not None:
                        if not self.filter(obj):
                            continue

                except json.decoder.JSONDecodeError:
                    continue

                yield tuple(obj[key] for key in self.output if key in obj)
