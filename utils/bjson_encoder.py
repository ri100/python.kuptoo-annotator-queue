class BJsonEncoder:

    def __init__(self):
        self.dictionary = {}

    def encode(self, dictionary):
        for key in dict(dictionary).keys():
            enc_key = self._encode_key(key)
            dictionary[enc_key] = dictionary[key]
            if enc_key != key:
                del(dictionary[key])
            if isinstance(dictionary[enc_key], dict):
                self.encode(dictionary[enc_key])

        return dictionary

    @staticmethod
    def _encode_key(key):
        return key.replace("\\", "_").replace("\$", "_").replace(".", "_")
