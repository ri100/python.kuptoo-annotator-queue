from annotator_enums.enums import Key

database_mapping = {
    "index": "kuptoo",
    "body": {
        "index_patterns": [
            "kuptoo-*"
        ],
        "settings": {
            "number_of_shards": 5,
            "number_of_replicas": 1
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
                "meta": {
                    "properties": {
                        "active": {
                            "type": "boolean",
                            "index": True
                        },
                        "inserted": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                        "updated": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                        "deleted": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                    },
                },
                "offer": {
                    "properties": {
                        "title": {
                            "properties": {
                                "raw": {
                                    "type": "text",
                                    "index": False
                                },
                                "std": {
                                    "type": "text",
                                    "index": True
                                },
                            }
                        },
                        "url": {
                            "type": "keyword",
                            "index": False
                        },
                        "description": {
                            "type": "text",
                            "index": True
                        },
                        "price": {
                            "properties": {
                                "gross": {
                                    "properties": {
                                        "amount": {
                                            "type": "float",
                                            "index": True
                                        },
                                        "currency": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "prev": {
                                            "properties": {
                                                "amount": {
                                                    "type": "float",
                                                    "index": False
                                                },
                                                "currency": {
                                                    "type": "keyword",
                                                    "index": False
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "photos": {
                            "properties": {
                                "local": {
                                    "properties": {
                                        "default": {
                                            "type": "integer",
                                            "index": False
                                        },
                                        "paths": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                },
                                "remote": {
                                    "properties": {
                                        "default": {
                                            "type": "integer",
                                            "index": False
                                        },
                                        "urls": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                }
                            }
                        },
                        "availability": {
                            "properties": {
                                "stock": {
                                    "type": "integer",
                                    "index": True
                                },
                                "remote": {
                                    "type": "boolean",
                                    "index": True
                                }
                            }
                        },
                        "attr": {
                            "type": "nested",
                            "properties": {
                                "key": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "value": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "producer": {
                            "properties": {
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "country": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "supplier": {
                            "properties": {
                                "id": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "website": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "gid": {
                            "properties": {
                                "local": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "remote": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "ean": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "isbn": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "redisKey": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "partNo": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "context": {
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "index": True
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "probability": {
                                    "type": "float",
                                    "index": True
                                },
                            }
                        },
                        "category": {
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "index": True
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "probability": {
                                    "type": "float",
                                    "index": True
                                },
                                "valid": {
                                    "type": "boolean",
                                    "index": True
                                },
                                "vector": {
                                    "type": "dense_vector",
                                    "dims": 300
                                }
                            }
                        },
                        "labels": {
                            "properties": {
                                "context": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "products": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "action": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "rel": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                }
                            }
                        },
                        "tags": {
                            "properties": {
                                "version": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "auto": {
                                    "properties": {
                                        'products_1': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 2
                                        },
                                        Key.WYTWORY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.MARKETING: {
                                            "type": "keyword",
                                            "index": False,
                                            "boost": 0.1
                                        },
                                        Key.MARKA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        'adj': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.3
                                        },
                                        'products_2': {
                                            "type": "text",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.REL_ANTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_BEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_OD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_O: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_POD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZECIW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_W: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_Z: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        }
                                    }
                                },
                                "stem": {
                                    "properties": {
                                        'products_1': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 2
                                        },
                                        Key.WYTWORY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        'products_2': {
                                            "type": "text",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        'adj': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.3
                                        },
                                        Key.REL_ANTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_BEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_OD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_O: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_POD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZECIW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_W: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_Z: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        }
                                    }
                                },
                            }
                        },
                        "hash": {
                            "properties": {
                                "raw": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                },
                                "std": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
