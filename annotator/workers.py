import hashlib, logging, pytz, time, config
from annotator_nlp.preprocessing import standardize, tokenize_sentence
from kombu import Exchange, Queue, Connection
from kombu.mixins import ConsumerMixin
from kuptoo_annotator.annotator import Annotator
from kuptoo_annotator.classifier import Classifier, Category, CategoryPath
from kuptoo_annotator.corrector import Corrector
from kuptoo_annotator.labeler import Labeler
from kuptoo_annotator.model import NoPosModel, ContextModel, PosModel
from kuptoo_annotator.utils.stemmer import stem
from kuptoo_annotator.utils.tags import basic_tag_keys

from elastic.storage import Elastic
from datetime import datetime, timezone

logger = logging.getLogger('annotator-worker')


# class PersisterWorker(ConsumerMixin):
#
#     def __init__(self, connection, index, skip_existing=False):
#         self.skip_existing = skip_existing
#         exchange = Exchange(config.rabbit['persist.queue'], 'direct', durable=True)
#         self.queue = Queue(config.rabbit['persist.queue'], exchange=exchange, routing_key='offer.TAG')
#         self.storage = Elastic(config.elastic, index)
#         self.connection = connection
#         self.batch_size = 1000
#         self.all_records = 0
#         self._reset()
#         self.skipped = 0
#         logger.debug("Batch size set to {}".format(self.batch_size))
#
#     def _reset(self):
#         self.batch_counter = self.batch_size
#         self.batch = []
#
#     def get_consumers(self, Consumer, channel):
#         return [
#             # There may be several consumers
#             Consumer(queues=[self.queue], callbacks=[self.handle_offer],
#                      auto_declare=True, prefetch_count=20,
#                      tag_prefix='persister-worker-'),
#         ]
#
#     def _exists(self, id):
#         return id in self.storage
#
#     def handle_offer(self, payload_in, message):
#         for row in payload_in:
#             self.all_records += 1
#
#             raw_hash = row['offer']['hash']['raw']
#
#             self.batch_counter -= 1
#
#             if self.skip_existing and self._exists(raw_hash):
#                 self.skipped += 1
#                 continue
#
#             today = datetime.now(timezone.utc)
#             cet = pytz.timezone('Europe/Warsaw')
#
#             row['meta'] = {
#                 'active': False,
#                 'inserted': today.astimezone(cet).strftime("%Y-%m-%d %H:%I:%S %Z")
#             }
#
#             self.batch.append(row)
#
#             if self.batch_counter <= 0:
#                 r = self.storage.insert(self.batch)
#                 logger.debug("Index result {}".format(r))
#                 self._reset()
#                 self.skipped = 0
#
#         message.ack()
#
#     def close(self):
#         logging.info('Saving remaining: {} ...'.format(len(self.batch)))
#         self.storage.insert(self.batch)
#         self._reset()


# class AnnotatorWorker(ConsumerMixin):
#
#     def __init__(self, connection, progress=0, remapping: dict = None, skip_existing=False):
#         self.vectorizer = ContextModel().get_latent_encoder()
#         self.classifier = Classifier(ContextModel())
#         self.labeler = Labeler(min_probability=.3)
#         self.annotator = Annotator(PosModel(), remapping)
#         self.skip_existing = skip_existing
#         self.progress = progress
#         self.i = 0
#         self.time = time.time()
#         self.exchange_insert = Exchange(config.rabbit['insert.queue'], 'direct', durable=True)
#         self.queue_insert = Queue(config.rabbit['insert.queue'], exchange=self.exchange_insert, routing_key='offer.NEW')
#         self.connection = connection
#         self.exchange_tags = Exchange(config.rabbit['persist.queue'], 'direct', durable=True)
#         self.queue_tags = Queue(config.rabbit['persist.queue'], exchange=self.exchange_tags, routing_key='offer.TAG')
#         logger.debug('Constructor ends...')
#
#     def get_consumers(self, consumer, channel):
#         logger.debug('Consumers loaded...')
#         return [
#             consumer(queues=[self.queue_insert], callbacks=[self.handle_offer],
#                      auto_declare=True, prefetch_count=20,
#                      tag_prefix='annotator-worker-'),
#         ]
#
#     @staticmethod
#     def _get_hash(string):
#         h = hashlib.md5()
#         h.update(string.encode('utf-8'))
#         return h.hexdigest()
#
#     @staticmethod
#     def _standardize(text):
#         text = standardize(text.lower())
#         return " ".join(tokenize_sentence(text, True))
#
#     @staticmethod
#     def _transform_label(key, labels):
#         return {
#             key: {
#                 'id': [label['id'] for label in labels[key]],
#                 'name': [label['name'] for label in labels[key]],
#                 'probability': [float("{0:.1f}".format(label['probability'] * 100)) for label in labels[key]],
#             }
#         }
#
#     def handle_offer(self, payload_in, message):
#         with Connection(config.rabbit['host'], connect_timeout=120) as local_connection:
#
#             standardized_texts = [self._standardize(row['offer']['title']['raw']) for row in payload_in]
#
#             labels_vectors = list(self.labeler.label(standardized_texts))
#             category_paths = list(self.classifier.categorize(standardized_texts))  # type: list[CategoryPath]
#             tags = self.annotator.tag(standardized_texts, allowed_tags=basic_tag_keys)
#             tags = list(Corrector().correct(tags))
#             stems = [stem(t) for t in tags]
#
#             payload_out = []
#             for position, row in enumerate(payload_in):
#                 title = row['offer']['title']['raw']
#                 std = standardized_texts[position]
#
#                 hash = {
#                     'hash': {
#                         'raw': self._get_hash(title),
#                         'std': self._get_hash(std)
#                     }
#                 }
#
#                 title = {
#                     'title': {
#                         'raw': title,
#                         'std': std
#                     }
#                 }
#
#                 _, labels = labels_vectors[position]
#                 # labels = [[label['id'], label['name'], float("{0:.1f}".format(label['probability'] * 100))] for
#                 #           label in labels]
#
#                 _labels = {'labels': {}}
#                 for key in ['context', 'products', 'action', 'rel']:
#                     _labels['labels'][key] = self._transform_label(key, labels)
#
#                 # _labels = {
#                 #     'label': {
#                 #         'id': [label['id'] for label in labels],
#                 #         'name': [label['name'] for label in labels],
#                 #         'probability': [float("{0:.1f}".format(label['probability'] * 100)) for label in labels],
#                 #     }
#                 # }
#
#                 category_path, vector = category_paths[position]  # type: CategoryPath, ndarray
#                 _category = {
#                     'category': {
#                         'id': [int(cat.cidx) for cat in category_path.path[1:]],
#                         'name': [cat.name for cat in category_path.path[1:]],
#                         'probability': [float(cat.probability) for cat in category_path.path[1:]],
#                         'valid': [bool(category_path.verify(1)), bool(category_path.verify(2))],
#                         'vector': vector.tolist()
#                     }
#                 }
#                 context = category_path.path[0]
#                 _context = {
#                     'context': {
#                         'id': int(context.cidx),
#                         'name': context.name,
#                         'probability': float(context.probability),
#                     }
#                 }
#
#                 if tags[position]:
#                     auto_tags = {
#                         'tags': {
#                             'auto': tags[position],
#                             'stem': stems[position]
#                         }
#                     }
#                     row['offer'].update(auto_tags)
#
#                 row['offer'].update(title)
#                 row['offer'].update(_labels)
#                 row['offer'].update(_category)
#                 row['offer'].update(_context)
#                 row['offer'].update(hash)
#
#                 payload_out.append(row)
#
#             self.i += 1
#
#             # Queue
#             producer = local_connection.Producer(serializer='json')
#             producer.publish(payload_out,
#                              exchange=self.exchange_tags, routing_key='offer.TAG',
#                              declare=[self.queue_tags])  # , compression='bzip2'
#         message.ack()
#
#         if self.progress > 0 and self.i % self.progress == 0:
#             elapsed = time.time() - self.time
#             logger.info("Average time: {},".format(self.i / elapsed))
