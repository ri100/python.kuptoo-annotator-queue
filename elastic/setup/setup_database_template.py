from elasticsearch import Elasticsearch
from elastic.indices.database_mapping import database_mapping
from elastic.utils import ElasticIndex
from config import elastic as config

es = Elasticsearch([{'host': config['host'], 'port': config['port']}])
idx = ElasticIndex(es, 'kuptoo')
# idx.del_template('kuptoo-*')
result = idx.put_template('kuptoo-*', database_mapping['body'])
print(idx.get_template('kuptoo-*'))
print(result)
