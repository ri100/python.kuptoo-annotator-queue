import hashlib
import elasticsearch
from elasticsearch import Elasticsearch, helpers


class Elastic:

    def __init__(self, config, index):
        self._index = index
        self._cache = {}
        self._config = config
        self._client = Elasticsearch([{'host': config['host'], 'port': config['port']}])

    def _get_hash(self, string):
        h = hashlib.md5()
        h.update(string.encode('utf-8'))
        return h.hexdigest()

    def __contains__(self, item):
        try:
            if item in self._cache:
                return True
            self._cache[item] = self[item]
            return True
        except elasticsearch.exceptions.NotFoundError:
            self._cache = {}
            return False

    def __getitem__(self, item):
        if item in self._cache:
            return self._cache[item]
        self._cache[item] = self._client.get(index=self._index, id=item)
        return self._cache[item]

    def clear(self):
        self._cache = {}

    def insert(self, records):
        bulk = []
        for record in records:
            bulk.append({
                "_index": self._index,
                "_id": record['offer']['hash']['raw'],
                "_source": record
            })
        return helpers.bulk(self._client, bulk)


if __name__=="__main__":
    import config

    ela = Elastic(config.elastic, 'kuptoo-allegro-1')
    print('215099ee651f05483289eee1f0d921d3' in ela)
    print(ela['215099ee651f05483289eee1f0d921d3'])
