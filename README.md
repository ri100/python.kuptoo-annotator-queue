# Installation


    //sudo apt-get -f install librocksdb-dev libsnappy-dev zlib1g-dev libbz2-dev libgflags-dev liblz4-dev
    //sudo apt-get -f install libaspell-dev aspell-pl
    sudo apt-get -f install python3-pip
    
    pip3 install -r requirements.txt 
    //pip3 install nltk
    //pip3 install git+https://ri100@bitbucket.org/ri100/python.annotator-dbs.git@1.3
    //pip3 install git+https://ri100@bitbucket.org/ri100/python.annotator-lib.git

# Elasticsearch cluster instalacja proxmox


 ## Konfifuracja systemu


 ### Swap

 Wyłącz swap na wszystkich maszynach: 
	```
	sudo swapoff -a
	```
 Więcej informacji:
 https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration-memory.html#bootstrap-memory_lock

 ### Files Descriptors

 na maszynie na której jest proxmox trzeba zwiększyć ilość fd aby mogły się one spropagować do kontenerów
 na pve-01 w shell wykonaj:


 edutuj /etc/security/limits.conf id dodaj:
 
 ```
 elasticsearch  -  nofile  65535
 ```

 EDYTUJ /etc/sysctl.conf 
 

 dodaj:
 ```
 fs.file-max = 65535 
 ```

 na pve-01 w /etc/pve/lxc/nr-kontenera.conf dodaj:

 ```
 lxc.prlimit.nofile: 65535 
 ```

 REBOOT


 sprawdz czy zmiany sie spropagowały:
 ```
	ulimit -Hn
 ```

 więcej: https://www.elastic.co/guide/en/elasticsearch/reference/current/setting-system-settings.html#limits.conf
 
 ### Pamięc wirtualna

 Wiecej: https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html


 ### Konfiguracja Heap
 
 nano /etc/elasticsearch/jvm.options


 ```
 -Xms2g
 -Xmx2g
 ```

## Instalacja elastic

Potrzebujemy trzech maszyn na których zainstalujemy elastica.
```
Zainstaluj javę:
apt update
apt install openjdk-11-jre-headless
apt install gnupg2
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt update
apt install elasticsearch
/bin/systemctl daemon-reload
/bin/systemctl enable elasticsearch.service
systemctl start elasticsearch.service

jeżeli wystąpią jakieś problemy z zestawieniem klastra wykasuj katalog z nodes z /var/lib/elasticsearch lub z nowe skonfigurowej ścieżki  w /etc/elastic/elasticsearch.conf
pamiętaj że prawadstępu do /var/lib/elasticsearch muszą być elasticsearch:elasticsearch

```

