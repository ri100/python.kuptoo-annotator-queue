#!/usr/bin/env bash

sudo cp annotatord_venv.service /etc/systemd/system/annotatord_venv.service
sudo chown root:root /etc/systemd/system/annotatord_venv.service
sudo chmod 664 /etc/systemd/system/annotatord_venv.service

sudo systemctl daemon-reload

