from elasticsearch import Elasticsearch
from elastic.indices.product_embeddings import product_mapping
from elastic.utils import ElasticIndex
from config import elastic as config

es = Elasticsearch([{'host': config['host'], 'port': config['port']}])
idx = ElasticIndex(es, 'embedding')
# idx.del_template('embedding-products')
result = idx.put_template('embedding-*', product_mapping['body'])
print(idx.get_template('embedding-*'))
print(result)