product_mapping = {
    "index": "embedding",
    "body": {
        "index_patterns": [
            "embedding-*"
        ],
        "settings": {
            "number_of_shards": 5,
            "number_of_replicas": 1
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
                "product": {
                    "properties": {
                        "name": {
                            "type": "text",
                            "index": True

                        },
                        "vector": {
                            "type": "dense_vector",
                            "dims": 96
                        }
                    },

                },
            }
        }
    }
}
