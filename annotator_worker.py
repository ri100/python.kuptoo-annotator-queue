import logging
import config
import argparse
import annotator
# import annotator_dbs.utils.lazy
from kombu import Connection
from annotator.worker.annotator import AnnotatorWorker

logging.getLogger('elasticsearch').setLevel(logging.ERROR)
logger = logging.getLogger('annotator-cmd')

parser = argparse.ArgumentParser(description='Annotation worker.')
parser.add_argument('-p', '--progress_every', help='Display progress every records.', type=int, default=0,
                    required=False)
parser.add_argument('--debug', help="Add debug information", required=False, action='store_true')
args = parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.INFO)
    logger.setLevel(logging.DEBUG)
    annotator.workers.logger.setLevel(logging.DEBUG)
    # annotator_dbs.utils.lazy.logger.setLevel(logging.DEBUG)

if args.progress_every == 0:
    logger.debug("No progress display...")

progress = args.progress_every if args.progress_every is not None else 0

with Connection(config.rabbit['host'], connect_timeout=20) as conn:
    try:

        worker = AnnotatorWorker(conn, progress, remapping={'_': "adj"})
        worker.run()
    except KeyboardInterrupt:
        logger.debug('Bye bye')
