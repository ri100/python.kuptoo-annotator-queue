from elasticsearch import Elasticsearch
from elastic.indices.corpus_mapping import corpus_mapping
from elastic.utils import ElasticIndex
from config import elastic as config

es = Elasticsearch([{'host': config['host'], 'port': config['port']}])
idx = ElasticIndex(es, 'corpus')
result = idx.put_template('corpus-*', corpus_mapping['body'])
print(idx.get_template('corpus-*'))
print(result)
